<?php
trait Hello {
    public function sayHello() {
        echo 'Hello ';
    }
}

trait World {
    public function sayWorld() {
        echo 'World';
    }
}

trait MyHelloWorld {
    use Hello, World;

    public function sayExclamationMark() {
        echo ' !';
    }
}

class MyClass{

    use MyHelloWorld;

}

$o = new MyClass();
$o->sayHello();
$o->sayWorld();
$o->sayExclamationMark();
