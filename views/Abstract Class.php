<?php

abstract class MyAbstractClass{


    protected $name;

    abstract public function getData();

    abstract public function settData($value);

    public function disPlayData(){

        echo "Name: ". $this->name;
    }

}// end of MyAbstractClass

class MyClass extends MyAbstractClass{

     public function getData()
     {
         return $this->name;
     }

public function settData($value)
{
    $this->name= $value;
}
} // End of MyClass

$o= new MyClass();

$o->settData("BITM");

echo $o->disPlayData();