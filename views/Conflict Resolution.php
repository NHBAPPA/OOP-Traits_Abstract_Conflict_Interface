<?php
trait A{

    public function smallTalk(){
        echo "a";
    }

    public function mediumTalk(){
        echo "aA";
    }
    public function bigTalk(){
        echo ' A';

    }
}

trait B{

    public function smallTalk(){
        echo "b";
    }

    public function mediumTalk(){
        echo " bB ";
    }
    public function bigTalk(){
        echo 'B';

    }
}

trait C{

    public function smallTalk(){
        echo "c";
    }

    public function mediumTalk(){
        echo "cC";
    }
    public function bigTalk(){
        echo ' C';

    }
}

class MyClass{

    use A,B,C{

        A::smallTalk insteadof B,C;
        B::mediumTalk insteadof A,C;
        C::bigTalk insteadof A,B;

        A::bigTalk as bigTalkA; //result=2
    }

}

$o= new MyClass();
$o->smallTalk();
$o->mediumTalk();
$o->bigTalk();
$o->bigTalkA(); //result=2

//Result 1 = abBC //
//Result 2 = abBCA //